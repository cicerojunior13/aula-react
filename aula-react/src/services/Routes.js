import React from 'react'
import {BrowserRouter, Switch, Route, Link} from 'react-router-dom'
import Cotacao from '../pages/cotacao/cotacao.jsx'
import Inicial from '../pages/inicial/inicial.jsx'


function Routes(){
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/cotacao">
                    <Cotacao></Cotacao>
                </Route>
                <Route exact path="/inicial">
                    <Inicial></Inicial>
                </Route>
                <Route exact path="/working">
                    <Working></Working>
                </Route>
            </Switch>
        </BrowserRouter>

    )
}

export default Routes