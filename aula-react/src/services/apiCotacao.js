import axios from 'axios'

const api = axios.create({
    baseURL: "https://economia.awesomeapi.com.br/"   
})

let config = {
    headers: {

    }
}

async function valores(){

    await api.get("/all", config)
        .then(response => {
            console.log(response.data)
        }).catch(error => {
            console.log(error)
        })
}

export {valores}